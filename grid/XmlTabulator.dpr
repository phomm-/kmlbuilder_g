program XmlTabulator;

uses
  Forms,
  MainForm in 'MainForm.pas' {TheMainForm},
  Tabulator in 'Tabulator.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainForm, TheMainForm);
  Application.Run;
end.

