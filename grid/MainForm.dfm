object MainForm: TMainForm
  Left = 0
  Top = 0
  ClientHeight = 282
  ClientWidth = 418
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Grid: TStringGrid
    Left = 0
    Top = 0
    Width = 418
    Height = 282
    Align = alClient
    FixedCols = 0
    TabOrder = 0
    ExplicitHeight = 320
  end
  object MainMenu: TMainMenu
    Left = 8
    Top = 32
    object miFile: TMenuItem
      Caption = 'File'
      object miOpen: TMenuItem
        Caption = 'Open'
        OnClick = miOpenClick
      end
    end
  end
  object dlgOpen: TOpenDialog
    Filter = 'XML-files(.xml)|*.xml'
    Left = 200
    Top = 160
  end
end
