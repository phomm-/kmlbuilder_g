unit MainForm;

interface

uses

  Windows, SysUtils, Variants, Classes, Controls, Forms, Dialogs, Menus, Grids;

type
  TMainForm = class(TForm)
    dlgOpen: TOpenDialog;
    Grid: TStringGrid;
    MainMenu: TMainMenu;
    miFile: TMenuItem;
    miOpen: TMenuItem;
    procedure miOpenClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    procedure DoOpen(const AFileName: string);
  end;

var
  TheMainForm: TMainForm;

implementation

uses
  XMLDoc, XMLIntf, Tabulator;

{$R *.dfm}

const
  SFFileLoadFailed = '������ �������� ����� %s'#13#10'�������: %s';
  Title = '��������� XML-�����';

procedure TMainForm.DoOpen(const AFileName: string);
var
  Doc: TXMLDocument;
begin
  Doc := TXMLDocument.Create(nil);
  try
    Doc.LoadFromFile(AFileName);
    Process(Doc.XML.Text, Grid);
  except
    on Ex: Exception do
    begin
      MessageDlg(Format(SFFileLoadFailed, [AFileName, Ex.Message]), mtError, [mbOK], 0);
      Grid.RowCount := 2;
      Grid.Rows[1].Clear();
    end;
  end;
  Caption := Title + ' - ' + AFileName;
  Doc.Free;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  Caption := Title;
  Application.Title := Title;
  InitGrid(Grid);
end;

procedure TMainForm.miOpenClick(Sender: TObject);
begin
  if dlgOpen.Execute then
    DoOpen(dlgOpen.FileName);
end;

end.

