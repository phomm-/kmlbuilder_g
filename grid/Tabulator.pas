unit Tabulator;

interface

uses
  Grids;

  procedure InitGrid(AGrid: TStringGrid);
  procedure Process(const AXMLContent: string; AGrid: TStringGrid);

implementation

uses
  XMLDoc, XMLIntf, Math, Classes, Variants;

procedure InitGrid(AGrid: TStringGrid);
begin
  AGrid.RowCount := 2;
  AGrid.FixedRows := 1;
  AGrid.FixedCols := 0;
  AGrid.Options := AGrid.Options + [goThumbTracking, goColSizing];
end;

procedure Process(const AXMLContent: string; AGrid: TStringGrid);
var
  NodeCount, C, R: Integer;
  Node: IXMLNode;
  AXMLDoc: TXmlDocument;
  cmp: TComponent;
  procedure SetupGrid(ANodeCount: Integer; Attribs: IXMLNodeList; WithClear: Boolean);
  var
    I: Integer;
  begin
    AGrid.RowCount := Max(ANodeCount + 1, 2);
    if Assigned(Attribs) then
    begin
      AGrid.ColCount := Attribs.Count;
      for I := 0 to Attribs.Count - 1 do
        AGrid.Cells[I, 0] := Attribs[I].NodeName;
    end;
    if WithClear then
      AGrid.Rows[1].Clear();
  end;
begin
  // workaround for using TXMLDocument instead of IXMLDocument
  // http://stackoverflow.com/questions/1532353/issue-building-an-xml-document-using-txmldocument
  cmp := TComponent.Create(nil);
  AXMLDoc := TXMLDocument.Create(cmp);
  AXMLDoc.LoadFromXML(AXMLContent);
  NodeCount := AXMLDoc.DocumentElement.ChildNodes.Count;
  Node := AXMLDoc.DocumentElement.ChildNodes.First;
  if Assigned(Node) then
    SetupGrid(NodeCount, Node.AttributeNodes, True);
  R := 1;
  while R <= NodeCount do
  begin
    for C := 0 to Node.AttributeNodes.Count - 1 do
      AGrid.Cells[C, R] := VarToStrDef(Node.AttributeNodes[C].NodeValue, '');
    Inc(R);
    Node := Node.NextSibling;
    if not Assigned(Node) then
      Break;
  end;
  SetupGrid(R - 1, nil, False);
  AXMLDoc.Free;
  cmp.Free;
end;

end.

