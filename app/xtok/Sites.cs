﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace xtok
{
    [Serializable]
    [XmlRoot("sites")]
    public class Sites 
    {
        [XmlElement("cell_layers")]
        public CellLayers CellLayers { get; set; }
        [XmlElement("site")]
        public List<Site> sites { get; set; }
    }
}
