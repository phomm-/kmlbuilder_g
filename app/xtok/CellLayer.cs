﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace xtok
{
    [Serializable]
    public class CellLayer
    {
        [XmlAttribute]
        public int id { get; set; }
        [XmlAttribute]
        public string name { get; set; }
        [XmlAttribute]
        public int length { get; set; }
        [XmlAttribute]
        public int color { get; set; }
    }
}
