﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace xtok
{
    [Serializable]
    public class CellLayers
    {
        [XmlAttribute]
        public int length { get; set; }
        [XmlAttribute]
        public int color { get; set; }
        [XmlElement("cell_layer")]
        public List<CellLayer> Layers { get; set; }
    }
}
