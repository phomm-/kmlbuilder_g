﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace xtok
{
    [Serializable]
    public class Antenna
    {
        [XmlAttribute]
        public int id { get; set; }
        [XmlAttribute]
        public int cell_layer { get; set; }
        [XmlAttribute]
        public string name { get; set; }

        [XmlAttribute]
        public int azimuth { get; set; }

        [XmlAttribute]
        public int height { get; set; }
    }
}
