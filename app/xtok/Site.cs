﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace xtok
{
    [Serializable]
    public class Site
    {
        [XmlAttribute]
        public int id { get; set; }
        [XmlAttribute]
        public string name { get; set; }
        [XmlAttribute]
        public string address { get; set; }
        [XmlAttribute]
        public double lat { get; set; }
        [XmlAttribute]
        public double lon { get; set; }
        [XmlElement("antenna")]
        public List<Antenna> Antennas { get; set; }
    }
}
