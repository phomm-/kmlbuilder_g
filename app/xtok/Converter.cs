﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;

namespace xtok
{
    using SharpKml;
    using SharpKml.Base;
    using SharpKml.Dom;
    using SharpKml.Engine;


    using System.Xml;
    using System.Xml.Serialization;
    internal class Converter
    {
        public const int PlaceRingPointCount = 4; // not incl endpoint which is the same as first
        public const int AntennaRingPointCount = 8; // --//--
        public const double LatRingScale = 0.00007;
        public const double LonRingScale = 0.00013;
        public const double LatArrowScale = 0.000001;
        public const double LonArrowScale = 0.0000015;
        public const int AntennaExtraHeight = 10;
        public const int DefaultAntennaLength = 5;
        public const int DefaultTilt = 45;
        public const int DefaultRange = 160;
        public const int TrialLimitation = 3; // 0 for no limit
        public const string LayerStyleNamePrefix = "cell_layer_";

        public static List<System.Drawing.Point> ArrowPoints = new List<System.Drawing.Point>(){
            new System.Drawing.Point(0, 0),
            new System.Drawing.Point(-1, 1),
            new System.Drawing.Point(-1, 6),
            new System.Drawing.Point(-3, 6),
            new System.Drawing.Point(0, 11),
            new System.Drawing.Point(3, 6),
            new System.Drawing.Point(1, 6),
            new System.Drawing.Point(1, 1),
            new System.Drawing.Point(0, 0)
        };

        private Kml Root;
        public KmlFile kml { get; private set; }

        private XmlDocument Doc = new XmlDocument();

        private XmlNode XmlRoot { get { return Doc.DocumentElement; } }
        public string ProjectName { get; private set; }
        
        public Sites Sites { get; private set; }

        public Converter()
        {
            Sites = new Sites();
        }

        public void LoadXml(string fileName)
        {
            Doc.Load(fileName);
        }

        private IEnumerable<Site> GetSites()
        {
            if (TrialLimitation == 0)
                return Sites.sites;
            else
                return Sites.sites.Take(TrialLimitation);
        }

        public void Parse()
        {
            ProjectName = XmlRoot["exportinfo"].Attributes["project_name"].InnerText;
            XmlSerializer sitesparser = new XmlSerializer(typeof(Sites));
            XmlNode node = XmlRoot["sites"];
            using (XmlReader reader = new XmlNodeReader(node))
            {                
                Sites = (Sites)sitesparser.Deserialize(reader);
            }
        }

        private Style GenStyle(bool children, string url)
        {
            Style style = new Style { List = new ListStyle { ItemType = children ? ListItemType.CheckHideChildren : ListItemType.Check } };
            ItemIcon openicon = new ItemIcon { State = ItemIconStates.Open, Href = new Uri(url) };
            ItemIcon closedicon = new ItemIcon { State = ItemIconStates.Closed, Href = openicon.Href };
            style.List.AddItemIcon(openicon);
            style.List.AddItemIcon(closedicon);
            return style;
        }

        public void Generate()
        {
            Root = new Kml();
            //Root.AddNamespacePrefix("xmlns", "http://earth.google.com/kml/2.2");
            Document doc = new Document() { Name = "Проект" };
            Root.Feature = doc;
            
            doc.AddStyle(new Style { Label = new LabelStyle { Color = new Color32(16777216) }, Id = "invisible_icon", Icon = new IconStyle { Color = new Color32(16777216), Icon = new IconStyle.IconLink(new Uri("http://maps.google.com/mapfiles/kml/paddle/wht-blank.png"))}, Line = new LineStyle { Color = new Color32(-1), Width = 2 } } );
            doc.AddStyle(new Style { Label = new LabelStyle { Color = new Color32(-1) }, Id = "description", Icon = new IconStyle { Color = new Color32(33554431), Icon = new IconStyle.IconLink(new Uri("http://maps.google.com/mapfiles/kml/paddle/wht-blank.png")) } });
            doc.AddStyle(new Style { Polygon = new PolygonStyle { Color = new Color32(-16755286), Fill = true, Outline = true }, Id = "property" });
            doc.AddStyle(new Style { Line = new LineStyle { Color = new Color32(-1), Width = 2 }, Id = "cell_to_property" });
            doc.AddStyle(new Style { Line = new LineStyle { Color = new Color32(-1), Width = 2 }, Id = "cell_to_ground" });
            doc.AddStyle(new Style { Polygon = new PolygonStyle { Color = new Color32(-65536), Fill = true, Outline = true }, Id = LayerStyleNamePrefix + "0" });
            if (Sites.CellLayers != null)
                foreach (CellLayer layer in Sites.CellLayers.Layers)
                    doc.AddStyle(new Style { Polygon = new PolygonStyle { Color = new Color32((int)(0xFF000000 + layer.color)), Fill = true, Outline = true }, Id = LayerStyleNamePrefix + layer.id.ToString() });

            Folder baseFolder = new Folder();
            baseFolder.Name = ProjectName;
            doc.AddFeature(baseFolder);
            Folder Places = new Folder { Name = "Площадки" };
            Folder Stations = new Folder { Name = "Базовые станции" };
            baseFolder.AddFeature(Places);
            baseFolder.AddFeature(Stations);

            foreach (Site site in GetSites())
            {
                LookAt look = new LookAt { Latitude = site.lat, Longitude = site.lon, Altitude = 0, Range = DefaultRange, Tilt = DefaultTilt, Heading = 0 };
                Folder placeFolder = new Folder { Name = site.name, Viewpoint = look };
                placeFolder.AddStyle(GenStyle(true, "http://www.rpls.ru/shared/property.bmp"));
                Vector pt = new Vector(site.lat, site.lon, 1); 
                Placemark mark = new Placemark { Name = site.name, StyleUrl = new Uri("#description", UriKind.Relative), Viewpoint = look, Geometry = new Point { Extrude = false, AltitudeMode = AltitudeMode.RelativeToGround, Coordinate = pt } };
                placeFolder.AddFeature(mark);
                LinearRing ring = new LinearRing { Coordinates = new CoordinateCollection() };                
                for (int i = 0; i <= PlaceRingPointCount; ++i)
                    ring.Coordinates.Add(new Vector(pt.Latitude + Math.Cos(2 * Math.PI * (i + 0.5) / PlaceRingPointCount) * LatRingScale, pt.Longitude + Math.Sin(2 * Math.PI * (i + 0.5) / PlaceRingPointCount) * LonRingScale, 2));
                mark = new Placemark { StyleUrl = new Uri("#property", UriKind.Relative), Geometry = new Polygon { Extrude = true, AltitudeMode = AltitudeMode.RelativeToGround, OuterBoundary = new OuterBoundary { LinearRing = ring } } };
                placeFolder.AddFeature(mark);
                
                Places.AddFeature(placeFolder);
            }

            foreach (Site site in GetSites())
            {
                Folder placeFolder = new Folder { Name = site.name };
                Vector pt = new Vector(site.lat, site.lon);
                placeFolder.AddStyle(GenStyle(false, "http://www.rpls.ru/shared/site.bmp"));   
                foreach (Antenna ant in site.Antennas)
                {
                    LookAt look = new LookAt { Latitude = site.lat, Longitude = site.lon, Altitude = ant.height + AntennaExtraHeight, Range = 50, Tilt = DefaultTilt, Heading = ant.azimuth };                
                    Folder antFolder = new Folder { Name = ant.name, Viewpoint = look };
                    antFolder.AddStyle(GenStyle(true, "http://www.rpls.ru/shared/cell.bmp"));
                    LinearRing ring = new LinearRing { Coordinates = new CoordinateCollection() };
                    double a = ant.azimuth * Math.PI / 180; // MathHelpers.DegreesToRadians(ant.azimuth);
                    CellLayer layer = Sites.CellLayers.Layers.FirstOrDefault(x => x.id == ant.cell_layer);
                    double len = layer == null ? DefaultAntennaLength : layer.length;
                    for (int i = 0; i <= AntennaRingPointCount; ++i)
                    {
                        //double a = ang;// -(ArrowPoints[i].X == 0 ? 0 : Math.Atan2(ArrowPoints[i].Y, ArrowPoints[i].X));
                        ring.Coordinates.Add(new Vector(
                            pt.Latitude + LatArrowScale * (ArrowPoints[i].X * Math.Sin(a) + ArrowPoints[i].Y * Math.Cos(a)) * len,
                            pt.Longitude - LonArrowScale * (ArrowPoints[i].X * Math.Cos(a) - ArrowPoints[i].Y * Math.Sin(a)) * len, ant.height));
                    }
                    Placemark mark = new Placemark { Geometry = new Polygon { AltitudeMode = AltitudeMode.RelativeToGround, OuterBoundary = new OuterBoundary { LinearRing = ring } } };                    
                    mark.StyleUrl = new Uri(/*layer != null ? layer.name : */ LayerStyleNamePrefix + ant.cell_layer.ToString(), UriKind.Relative);
                    antFolder.AddFeature(mark);
                    mark = new Placemark { StyleUrl = new Uri("#cell_to_ground", UriKind.Relative), Geometry = new LineString { AltitudeMode = AltitudeMode.RelativeToGround, Coordinates = new CoordinateCollection(new List<Vector> { new Vector(pt.Latitude, pt.Longitude, 0), new Vector(pt.Latitude, pt.Longitude, ant.height) }) } };
                    antFolder.AddFeature(mark);

                    placeFolder.AddFeature(antFolder);
                }
                Stations.AddFeature(placeFolder);
            } 
                        
        }

        public void SaveToFile(string fileName, bool doIndent)
        {
            kml = KmlFile.Create(Root, true);
            using (var stream = System.IO.File.OpenWrite(fileName))
            {
                stream.SetLength(0);                
                kml.Save(stream);
            }
            // kill indentation
            if (!doIndent)
            {
                XmlDocument doc = new XmlDocument();                
                doc.Load(fileName);                
                File.WriteAllText(fileName, ToUnIndentedString(doc), Encoding.UTF8);
            }
        }

        public static string ToUnIndentedString(XmlDocument doc)
        {
            StringWriterWithEncoding sw = new StringWriterWithEncoding(Encoding.UTF8);
            XmlWriterSettings settings = new XmlWriterSettings { Indent = false };
            using (XmlWriter writer = XmlWriter.Create(sw, settings))
            {
                doc.Save(writer);
            }
            return sw.ToString();
        }

        public sealed class StringWriterWithEncoding : StringWriter
        {
            private readonly Encoding encoding;

            public StringWriterWithEncoding(Encoding encoding)
            {
                this.encoding = encoding;
            }

            public override Encoding Encoding
            {
                get { return encoding; }
            }
        }
    }
}
