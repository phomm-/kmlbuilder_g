﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace xtok
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            if (true)
            {
                Run(@"f:\ph\order\kmlbuilder\files\sites.xml", @"f:\ph\order\kmlbuilder\files\1.kml", true);
            }
            SourceFileName = "";
        }

        private string sourceFileName;
        public string SourceFileName
        {
            get { return sourceFileName; }
            set
            {
                sourceFileName = value; 
                convertToolStripMenuItem.Enabled = value != "";
                convertToolStripButton.Enabled = value != "";
            }
        }

        private void buttonSource_Click(object sender, EventArgs e)
        {
            OpenFile();
        }

        private void OpenFile()
        {
            if (fileDialogSource.ShowDialog() == DialogResult.OK)
            {
                SourceFileName = fileDialogSource.FileName;
                textBox1.Text = File.ReadAllText(SourceFileName, Encoding.Default);
            }
        }

        private void OpenTarget()
        {
            if (fileDialogTarget.ShowDialog() == DialogResult.OK)
                textBoxTarget.Text = fileDialogTarget.FileName;
        }

        private void buttonTarget_Click(object sender, EventArgs e)
        {
            OpenTarget();
        }

        private void ConvertFile()
        {
            Run(SourceFileName, textBoxTarget.Text, checkBoxIndent.Checked);
        }


        private void buttonConvert_Click(object sender, EventArgs e)
        {
            ConvertFile();
        }

        private void Run(string source, string target, bool doIndent)
        {
            if (!File.Exists(source))
            {
                MessageBox.Show("Некорректное имя или путь исходного файла");
                return;
            }
            if (!Directory.Exists(Path.GetDirectoryName(source)))
            {
                MessageBox.Show("Некорректный путь выходного файла");
                return;
            }
            try
            {
                Converter con = new Converter();
                con.LoadXml(source);
                con.Parse();
                con.Generate();
                con.SaveToFile(target, doIndent);
                if (Converter.TrialLimitation != 0 && con.Sites.sites.Count > Converter.TrialLimitation)
                    MessageBox.Show(string.Format("Ограничение пробной версии.\n Сконвертировано только {0} площадок", Converter.TrialLimitation));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка, данные входного файла не предусмотрены программой\nПодробности: " + ex.Message);
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("О программе", "Программа конвертирует XML файлы в KML (файлы для работы Google Earth)\nПрограмма написана на заказ фрилансером phomm@mail.ru");
        }

        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFile();
        }

        private void convertToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (fileDialogTarget.ShowDialog() == DialogResult.OK)
            {
                textBoxTarget.Text = fileDialogTarget.FileName;
                ConvertFile();
            }
        }
    }
}
